<h1 align="center">React Creates ⚛️</h1>
<p align="center">
Simple and easy to use!
React creates for you useful and common tools that adapt themselves to your project for faster and easier development
</p>

<p align="center">
  <a href="https://github.com/tzachbon/react-creates/tree/master/packages/react-creates">
    <img src="https://img.shields.io/badge/Maintained%3F-yes-green.svg">
  </a>
  <a href="https://github.com/tzachbon/">
    <img src="https://img.shields.io/badge/Ask%20me-anything-1abc9c.svg">
  </a>
  <a href="https://marketplace.visualstudio.com/items?itemName=TzachBonfil.react-creates-vsc">
    <img src="https://vsmarketplacebadge.apphb.com/version/TzachBonfil.react-creates-vsc.svg">
  </a>
  <a href="https://github.com/tzachbon/react-creates/blob/master/LICENSE">
    <img src="https://img.shields.io/github/license/tzachbon/react-creates.svg">
  </a>
  <a href="https://GitHub.com/tzachbon/react-creates/tags/">
    <img src="https://img.shields.io/github/tag/tzachbon/react-creates.svg">
  </a>
</p>


You can use:

- [Super simple CLI](packages/react-creates/README.md)
- [Visual Studio Code Extension (VSCode)](packages/react-creates-vsc/README.md)

Examples:

> CLI

<p align="center">
  <img src="packages/react-creates/screencast.gif">
</p>

> VSCode

<p align="center">
  <img src="packages/react-creates-vsc/screencast.gif">
</p>
